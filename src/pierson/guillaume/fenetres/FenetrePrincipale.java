package pierson.guillaume.fenetres;


import pierson.guillaume.Reverso.entites.Client;
import pierson.guillaume.Reverso.entites.Prospect;
import pierson.guillaume.Reverso.entites.Societe;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class FenetrePrincipale extends JFrame implements ItemListener {
    private final String CLIENT_FILE = "client.sav";
    private final String PROSPECT_FILE = "prospect.sav";

    private JLabel lblBienvenue = new JLabel("Bienvenue sur la Gestion des Clients et Prospects REVERSO. ",SwingConstants.CENTER);

    private static JRadioButton optClient = new JRadioButton("Gestion des clients");
    private static JRadioButton optProspect = new JRadioButton("Gestion des prospects");

    private JButton cmdCreation = new JButton("Nouveau");
    private JButton cmdSuppression = new JButton("Supprimer");
    private JButton cmdModification = new JButton("Modifier");
    private JButton cmdAffichage = new JButton("Afficher");

    private JPanel panTableauClient = new JPanel();
    private JPanel panTableauProspect = new JPanel();

    private static JTable tblClient = new JTable();
    private static JTable tblProspect = new JTable();

    private static final String[] ENTETE_CLIENT = {"Id", "Raison social", "Domaine", "CA", "Nb Employés"};
    private static final String[] ENTETE_PROSPECT = {"Id", "Raison social", "Domaine", "Date prospection", "Interet"};

    private static DefaultTableModel tableModelClient = new DefaultTableModel(ENTETE_CLIENT, 0)
    {
        @Override
        public boolean isCellEditable(int row, int column)
        {
            return false;
        }
    };

    private static DefaultTableModel tableModelProspect = new DefaultTableModel(ENTETE_PROSPECT, 0)
    {
        @Override
        public boolean isCellEditable(int row, int column)
        {
            return false;
        }
    };

    private int indiclient = 0;
    private int indiprospect = 0;

    /**
     * Constructeur de la fentre principale
     */
    public FenetrePrincipale() throws IOException, ClassNotFoundException {
        this.setSize(800,600);
        this.setTitle("Reverso - Gestion des clients et prospects");

        ImageIcon icone = new ImageIcon("reverso.png");
        this.setIconImage(icone.getImage());

        this.setResizable(false);

        //this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        WindowListener wl = new WindowAdapter() {
            public void windowClosing(WindowEvent e){
                try {
                    clientExportToFile(Client.getClientList(), CLIENT_FILE);
                    prospectExportToFile(Prospect.getProspectList(), PROSPECT_FILE);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }finally {
                    System.exit(0);
                }

            }
        };

        addWindowListener(wl);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2 - this.getWidth()/2, dim.height/2 - this.getHeight()/2);

        init();
    }

    /**
     * Initialise les composants de la fenetre principale
     */
    private void init() throws IOException, ClassNotFoundException {
        JPanel panelSelection = new JPanel(new FlowLayout(FlowLayout.CENTER, 150,5));
        Border borderSelection = BorderFactory.createTitledBorder("Sélectionnez");
        panelSelection.setBorder(borderSelection);

        ButtonGroup groupSelection = new ButtonGroup();

        groupSelection.add(optClient);
        panelSelection.add(optClient);
        groupSelection.add(optProspect);
        panelSelection.add(optProspect);

        optClient.addItemListener(this);
        optProspect.addItemListener(this);

        JPanel panelAction = new JPanel(new FlowLayout(FlowLayout.CENTER, 50,5));
        Border borderAction = BorderFactory.createTitledBorder("Choisissez une action");
        panelAction.setBorder(borderAction);

        cmdCreation.setMnemonic('n');
        cmdAffichage.setMnemonic('a');
        cmdModification.setMnemonic('m');
        cmdSuppression.setMnemonic('s');

        cmdAffichage.addActionListener(e -> {
            afficher();});
        cmdCreation.addActionListener(e -> {
            creer();});
        cmdModification.addActionListener(e -> {
            modifier();});
        cmdSuppression.addActionListener(e -> {
            supprimer();});

        ButtonGroup groupAction = new ButtonGroup();

        groupAction.add(cmdCreation);
        panelAction.add(cmdCreation);
        groupAction.add(cmdAffichage);
        panelAction.add(cmdAffichage);
        groupAction.add(cmdModification);
        panelAction.add(cmdModification);
        groupAction.add(cmdSuppression);
        panelAction.add(cmdSuppression);

        Container contentPane = this.getContentPane();
        contentPane.add(panelSelection, BorderLayout.NORTH);
        contentPane.add(lblBienvenue, BorderLayout.CENTER);
        contentPane.add(panelAction, BorderLayout.SOUTH);

        clientImportFromFile(CLIENT_FILE);
        prospectImportFromFile(PROSPECT_FILE);
        setCompteur();
    }

    public static JTable getTblClient() {
        return tblClient;
    }

    public static JTable getTblProspect() {
        return tblProspect;
    }

    public static JRadioButton getOptClient() {
        return optClient;
    }

    public static JRadioButton getOptProspect() {
        return optProspect;
    }

    /**
     * Affichage du tableau de client
     *
     *
     */
    public void client(){
        lblBienvenue.setVisible(false);
        panTableauProspect.setVisible(false);

        if(indiclient == 0) {
            Border borderTableau = BorderFactory.createTitledBorder("Gestion des clients");
            panTableauClient.setBorder(borderTableau);

            for (Client i : Client.getClientList()) {
                tableModelClient.addRow(new Object[]{i.getIdentifiant(), i.getRaisonSocial(), i.getDomaineSociete(), i.getCA() + " €", i.getNbEmployes()});
            }

            tblClient.setModel(tableModelClient);
            tblClient.setRowSelectionAllowed(true);
            tblClient.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tblClient.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            tblClient.getColumnModel().getColumn(0).setPreferredWidth(35);
            tblClient.getColumnModel().getColumn(1).setPreferredWidth(205);
            tblClient.getColumnModel().getColumn(2).setPreferredWidth(60);
            tblClient.getColumnModel().getColumn(3).setPreferredWidth(75);
            tblClient.getColumnModel().getColumn(4).setPreferredWidth(75);

            panTableauClient.add(new JScrollPane(tblClient), BorderLayout.CENTER);

            getContentPane().add(panTableauClient);
            panTableauClient.setVisible(true);
            indiclient = 1;
        }else if (indiclient == 1){
            panTableauClient.setVisible(true);
        }
    }

    /**
     * Affichage du tableau de prospect
     *
     */
    public void prospect(){
        lblBienvenue.setVisible(false);
        panTableauClient.setVisible(false);

        if(indiprospect == 0) {
            Border borderTableau = BorderFactory.createTitledBorder("Gestion des prospects");
            panTableauProspect.setBorder(borderTableau);

            for (Prospect i : Prospect.getProspectList()) {
                tableModelProspect.addRow(new Object[]{i.getIdentifiant(), i.getRaisonSocial(), i.getDomaineSociete(), i.getDateProspection().format(Prospect.getFormatter()), i.getInteret()});
            }
            tblProspect.setModel(tableModelProspect);
            tblProspect.setRowSelectionAllowed(true);
            tblProspect.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tblProspect.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            tblProspect.getColumnModel().getColumn(0).setPreferredWidth(35);
            tblProspect.getColumnModel().getColumn(1).setPreferredWidth(210);
            tblProspect.getColumnModel().getColumn(2).setPreferredWidth(60);
            tblProspect.getColumnModel().getColumn(3).setPreferredWidth(95);
            tblProspect.getColumnModel().getColumn(4).setPreferredWidth(50);

            panTableauProspect.add(new JScrollPane(tblProspect), BorderLayout.CENTER);

            getContentPane().add(panTableauProspect);
            panTableauProspect.setVisible(true);
            indiprospect = 1;
        }else if (indiprospect == 1){
            panTableauProspect.setVisible(true);
        }

    }

    /**
     * Met à jour le tableau après une modification
     */
    public static void updateTableau(){
        if(optClient.isSelected()) {
            Collections.sort(Client.getClientList(), Client.getComparatorRaisonSocial());
            DefaultTableModel tableModelEmpty = (DefaultTableModel) tblClient.getModel();
            tableModelEmpty.setRowCount(0);

            for (Client i : Client.getClientList()) {
                tableModelClient.addRow(new Object[]{i.getIdentifiant(), i.getRaisonSocial(), i.getDomaineSociete(), i.getCA() + " €", i.getNbEmployes()});
            }

            tblClient.setModel(tableModelClient);


        } else if (optProspect.isSelected()) {
            Collections.sort(Prospect.getProspectList(), Prospect.getComparatorRaisonSocial());
            DefaultTableModel tableModelEmpty = (DefaultTableModel) tblProspect.getModel();
            tableModelEmpty.setRowCount(0);

            for (Prospect i : Prospect.getProspectList()) {
                tableModelProspect.addRow(new Object[]{i.getIdentifiant(), i.getRaisonSocial(), i.getDomaineSociete(), i.getDateProspection().format(Prospect.getFormatter()), i.getInteret()});
            }
            tblProspect.setModel(tableModelProspect);
        }

    }

    /**
     * Affiche un client ou un prospect selectionné dans le tableau
     */
    public void afficher(){
        if(optClient.isSelected()){
            if(tblClient.getSelectedRow() == -1){
                JOptionPane.showMessageDialog(null,Client.getClientList().get(0), "Client", JOptionPane.PLAIN_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, Client.getClientList().get(tblClient.getSelectedRow()), "Client", JOptionPane.PLAIN_MESSAGE);
            }
        } else if (optProspect.isSelected()) {
            if(tblProspect.getSelectedRow() == -1){
                JOptionPane.showMessageDialog(null, Prospect.getProspectList().get(0), "Prospect", JOptionPane.PLAIN_MESSAGE);
            }else {
                JOptionPane.showMessageDialog(null, Prospect.getProspectList().get(tblProspect.getSelectedRow()), "Prospect", JOptionPane.PLAIN_MESSAGE);
            }
        }else {
            JOptionPane.showMessageDialog(null,"Vous devez choisir un client ou un prospect.", "Erreur", JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * ouvre une fenetre de formulaire pour créer un client ou un prospect
     */
    public void creer(){
        FenetreSecondaire creer = new FenetreSecondaire(this);
        if(optClient.isSelected()){
            creer.initClient();
            creer.initCreer();
            creer.setVisible(true);
        } else if (optProspect.isSelected()) {
            creer.initProspect();
            creer.initCreer();
            creer.setVisible(true);
        }else {
            JOptionPane.showMessageDialog(this,"Vous devez choisir entre client et prospect.", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
        if (creer.isVisible()) {
            this.setVisible(false);
        }
    }

    /**
     * ouvre une fentre de formulaire pour modifier un client ou un prospect selectionné dans le tableau
     */
    public void modifier(){
        FenetreSecondaire modifier = new FenetreSecondaire(this);
        if(optClient.isSelected()){
            modifier.initClient();
            modifier.initModifier();
            modifier.setVisible(true);
        } else if (optProspect.isSelected()) {
            modifier.initProspect();
            modifier.initModifier();
            modifier.setVisible(true);
        }else {
            JOptionPane.showMessageDialog(this,"Vous devez choisir entre client et prospect.", "Erreur", JOptionPane.ERROR_MESSAGE);
        }

        if (modifier.isVisible()) {
            this.setVisible(false);
        }
    }

    /**
     * Supprime un client ou un prospect selectionné dans le tableau après une demande de confirmation
     */
    public void supprimer(){
        if(optClient.isSelected()){
            if(tblClient.getSelectedRow() == -1){
                JOptionPane.showMessageDialog(this,"Vous devez choisir un client.", "Erreur", JOptionPane.ERROR_MESSAGE);
            } else {
                int reponse = JOptionPane.showConfirmDialog(this,
                        "Etes vous certains de vouloir supprimer le client "+ Client.getClientList().get(tblClient.getSelectedRow()).getRaisonSocial() + " ?\nCette opération est définitive!", "Attention - Suppression",
                        JOptionPane.YES_NO_OPTION);
                if(reponse == JOptionPane.YES_OPTION){
                    Client.getClientList().remove(tblClient.getSelectedRow());
                    updateTableau();
                }
            }
        } else if (optProspect.isSelected()) {
            if(tblProspect.getSelectedRow() == -1){
                JOptionPane.showMessageDialog(this,"Vous devez choisir un prospect.", "Erreur", JOptionPane.ERROR_MESSAGE);
            }else {
                int reponse = JOptionPane.showConfirmDialog(this,
                        "Etes vous certains de vouloir supprrimer le prospect "+ Prospect.getProspectList().get(tblProspect.getSelectedRow()).getRaisonSocial() + " ?\nCette opération est définitive!", "Attention - Suppression",
                        JOptionPane.YES_NO_OPTION);
                if(reponse == JOptionPane.YES_OPTION){
                    Prospect.getProspectList().remove(tblProspect.getSelectedRow());
                    updateTableau();
                }
            }
        }else {
            JOptionPane.showMessageDialog(this,"Vous devez choisir un client ou un prospect.", "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Permet d'exporter la liste de clients dans un fichier
     * @param listClient
     * @param fileName
     * @throws IOException
     */
    public void clientExportToFile (ArrayList<Client> listClient, String fileName) throws IOException {
            FileOutputStream saveFile = new FileOutputStream(fileName);
            ObjectOutputStream save = new ObjectOutputStream(saveFile);
            save.writeObject(listClient);
            save.close();
    }

    /**
     * Permet d'exporter la liste de prospects dans un fichier
     * @param listProspect
     * @param fileName
     * @throws IOException
     */
    public void prospectExportToFile (ArrayList<Prospect> listProspect, String fileName) throws IOException {
        FileOutputStream saveFile = new FileOutputStream(fileName);
        ObjectOutputStream save = new ObjectOutputStream(saveFile);
        save.writeObject(listProspect);
        save.close();
    }

    public void setCompteur(){
        int compteurClient = 0;
        int compteurProspect = 0;
        for (Client i: Client.getClientList()) {
            if (i.getIdentifiant() > compteurClient){
                compteurClient = i.getIdentifiant();
            }
        }
        for (Prospect i : Prospect.getProspectList()) {
            if (i.getIdentifiant() > compteurProspect){
                compteurProspect = i.getIdentifiant();
            }
        }
        if(compteurClient>compteurProspect){
            Societe.setCompteurId(compteurClient+1);
        }else{
            Societe.setCompteurId(compteurProspect+1);
        }
    }

    /**
     * Permet d'importer la liste de clients depuis un fichier
     * @param fileName
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void clientImportFromFile (String fileName) throws IOException, ClassNotFoundException {
        FileInputStream saveFile = new FileInputStream(fileName);
        ObjectInputStream save = new ObjectInputStream(saveFile);
        Client.setClientList((ArrayList<Client>) save.readObject());
        save.close();
    }

    /**
     * Permet d'importer la liste de prospects depuis un fichier
     * @param fileName
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void prospectImportFromFile (String fileName) throws IOException, ClassNotFoundException {
        FileInputStream saveFile = new FileInputStream(fileName);
        ObjectInputStream save = new ObjectInputStream(saveFile);
        Prospect.setProspectList((ArrayList<Prospect>) save.readObject());
        save.close();
    }

    /**
     * Gere les actions liées à la selection des boutons radio client et prospect
     * @param e
     */
    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED){
            if(e.getItem() == optClient){
                client();
            }
            if(e.getItem() == optProspect){
                prospect();
            }
        }
    }

}
