package pierson.guillaume.fenetres;

import pierson.guillaume.Reverso.entites.Adresse;
import pierson.guillaume.Reverso.entites.Client;
import pierson.guillaume.Reverso.entites.Prospect;
import pierson.guillaume.exceptions.GestionReversoException;
import pierson.guillaume.types.DomaineSociete;
import pierson.guillaume.types.InteretProspect;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;


public class FenetreSecondaire extends JFrame {
    private JLabel lblId = new JLabel("Id : ");
    private JTextField txtId = new JTextField(5);
    private JLabel lblRaisonSocial = new JLabel("Raison social : ");
    private JTextField txtRaisonSocial = new JTextField(25);
    private JLabel lblDomaineSociete = new JLabel("Domaine : ");
    private JComboBox<DomaineSociete> cmbDomaineSociete = new JComboBox<>(DomaineSociete.values());
    private JLabel lblAdresse = new JLabel("Adresse : ");
    private JLabel lblNumRue = new JLabel("N° de rue : ");
    private JTextField txtNumRue = new JTextField(5);
    private JLabel lblNomRue = new JLabel("Nom de rue : ");
    private JTextField txtNomRue = new JTextField(50);
    private JLabel lblCodePostal = new JLabel("Code Postal : ");
    private JTextField txtCodePostal = new JTextField(7);
    private JLabel lblVille = new JLabel("Ville : ");
    private JTextField txtVille = new JTextField(25);
    private JLabel lblTelephone = new JLabel("N° de téléphone : ");
    private JTextField txtTelephone = new JTextField(12);
    private JLabel lblMail = new JLabel("Adresse mail : ");
    private JTextField txtMail = new JTextField(30);
    private JLabel lblCommentaire = new JLabel("Commentaires : ");
    private JTextArea txtCommentaire = new JTextArea(3,40);

    private JLabel lblCA = new JLabel("Chiffre d'affaire : ");
    private JTextField txtCA = new JTextField(10);
    private JLabel lblNbEmployes = new JLabel("Nombre d'employés : ");
    private JTextField txtNbEmployes = new JTextField(5);

    private JLabel lblDateProspection = new JLabel("Date de la dernière prospection : ");
    private JTextField txtDateProspection = new JTextField(10);
    private JLabel lblInteret = new JLabel("Interressé : ");
    private JComboBox<InteretProspect> cmbInteret = new JComboBox<>(InteretProspect.values());

    private JButton cmdValider = new JButton("Valider");
    private JButton cmdAnnuler = new JButton("Annuler");

    private JPanel panSociete = new JPanel();
    private JPanel panAdresse = new JPanel();
    private JPanel panCommande = new JPanel();

    private JPanel panId = new JPanel();
    private JPanel panRaisonSocial = new JPanel();
    private JPanel panDomaineSociete = new JPanel();
    private JPanel panTel = new JPanel();
    private JPanel panMail = new JPanel();
    private JPanel panCommentaire = new JPanel();
    private JPanel panCA = new JPanel();
    private JPanel panNbEmployes = new JPanel();
    private JPanel panDateProspect = new JPanel();
    private JPanel panInteretProspect = new JPanel();

    private JFrame homePage;

    /**
     * Constructeur de la fenetre de formulaire
     * prend une JFrame en param pour la lier à la fenetre principale
     * @param homePage
     */
    public FenetreSecondaire(FenetrePrincipale homePage){
        this.setSize(800,520);

        ImageIcon icone = new ImageIcon("reverso.png");
        this.setIconImage(icone.getImage());

        this.setResizable(false);

        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2 - this.getWidth()/2, dim.height/2 - this.getHeight()/2);

        this.homePage = homePage;
    }

    /**
     * Initialise les composants du formulaire pour un client
     */
    public void initClient(){
        this.setTitle("Client");

        panAdresse.setLayout(new BoxLayout(panAdresse, BoxLayout.Y_AXIS));
        panAdresse.setBorder(BorderFactory.createTitledBorder("Adresse"));

        panAdresse.add(lblNumRue);
        panAdresse.add(txtNumRue);
        panAdresse.add(lblNomRue);
        panAdresse.add(txtNomRue);
        panAdresse.add(lblCodePostal);
        panAdresse.add(txtCodePostal);
        panAdresse.add(lblVille);
        panAdresse.add(txtVille);

        panSociete.setLayout(new FlowLayout(FlowLayout.CENTER,70,20 ));
        panSociete.setBorder(BorderFactory.createTitledBorder("Société"));

        this.txtId.setEnabled(false);
        this.txtId.setDisabledTextColor(Color.BLACK);
        panId.setLayout(new BoxLayout(panId, BoxLayout.X_AXIS));
        panId.add(lblId);
        panId.add(txtId);
        panSociete.add(panId);

        panRaisonSocial.setLayout(new BoxLayout(panRaisonSocial, BoxLayout.X_AXIS));
        panRaisonSocial.add(lblRaisonSocial);
        panRaisonSocial.add(txtRaisonSocial);
        panSociete.add(panRaisonSocial);

        panDomaineSociete.setLayout(new BoxLayout(panDomaineSociete, BoxLayout.X_AXIS));
        panDomaineSociete.add(lblDomaineSociete);
        panDomaineSociete.add(cmbDomaineSociete);
        panSociete.add(panDomaineSociete);

        panSociete.add(panAdresse);

        panTel.setLayout(new BoxLayout(panTel, BoxLayout.X_AXIS));
        panTel.add(lblTelephone);
        panTel.add(txtTelephone);
        panSociete.add(panTel);

        panMail.setLayout(new BoxLayout(panMail, BoxLayout.X_AXIS));
        panMail.add(lblMail);
        panMail.add(txtMail);
        panSociete.add(panMail);

        panCA.setLayout(new BoxLayout(panCA, BoxLayout.X_AXIS));
        panCA.add(lblCA);
        panCA.add(txtCA);
        panSociete.add(panCA);

        panNbEmployes.setLayout(new BoxLayout(panNbEmployes, BoxLayout.X_AXIS));
        panNbEmployes.add(lblNbEmployes);
        panNbEmployes.add(txtNbEmployes);
        panSociete.add(panNbEmployes);

        txtCommentaire.setLineWrap(true);
        txtCommentaire.setWrapStyleWord(true);
        panCommentaire.setLayout(new BoxLayout(panCommentaire, BoxLayout.Y_AXIS));
        panCommentaire.add(lblCommentaire);
        panCommentaire.add(txtCommentaire);
        panSociete.add(panCommentaire);

        panCommande.setLayout(new FlowLayout(FlowLayout.CENTER, 200, 10));
        ButtonGroup groupCommand = new ButtonGroup();

        cmdValider.setMnemonic('v');
        cmdAnnuler.setMnemonic('a');

        groupCommand.add(cmdValider);
        panCommande.add(cmdValider);
        groupCommand.add(cmdAnnuler);
        panCommande.add(cmdAnnuler);

        Container contentPane = this.getContentPane();
        contentPane.add(panSociete, BorderLayout.CENTER);
        contentPane.add(panCommande, BorderLayout.SOUTH);

    }

    /**
     * Initialise les composants du formulaire pour un prospect
     */
    public void initProspect(){
        this.setTitle("Prospect");

        panAdresse.setLayout(new BoxLayout(panAdresse, BoxLayout.Y_AXIS));
        panAdresse.setBorder(BorderFactory.createTitledBorder("Adresse"));

        panAdresse.add(lblNumRue);
        panAdresse.add(txtNumRue);
        panAdresse.add(lblNomRue);
        panAdresse.add(txtNomRue);
        panAdresse.add(lblCodePostal);
        panAdresse.add(txtCodePostal);
        panAdresse.add(lblVille);
        panAdresse.add(txtVille);

        panSociete.setLayout(new FlowLayout(FlowLayout.CENTER,70,20 ));
        panSociete.setBorder(BorderFactory.createTitledBorder("Société"));

        this.txtId.setEnabled(false);
        this.txtId.setDisabledTextColor(Color.BLACK);
        panId.setLayout(new BoxLayout(panId, BoxLayout.X_AXIS));
        panId.add(lblId);
        panId.add(txtId);
        panSociete.add(panId);

        panRaisonSocial.setLayout(new BoxLayout(panRaisonSocial, BoxLayout.X_AXIS));
        panRaisonSocial.add(lblRaisonSocial);
        panRaisonSocial.add(txtRaisonSocial);
        panSociete.add(panRaisonSocial);

        panDomaineSociete.setLayout(new BoxLayout(panDomaineSociete, BoxLayout.X_AXIS));
        panDomaineSociete.add(lblDomaineSociete);
        panDomaineSociete.add(cmbDomaineSociete);
        panSociete.add(panDomaineSociete);

        panSociete.add(panAdresse);

        panTel.setLayout(new BoxLayout(panTel, BoxLayout.X_AXIS));
        panTel.add(lblTelephone);
        panTel.add(txtTelephone);
        panSociete.add(panTel);

        panMail.setLayout(new BoxLayout(panMail, BoxLayout.X_AXIS));
        panMail.add(lblMail);
        panMail.add(txtMail);
        panSociete.add(panMail);

        txtDateProspection.setToolTipText("jj-MM-aaaa");
        panDateProspect.setLayout(new BoxLayout(panDateProspect, BoxLayout.X_AXIS));
        panDateProspect.add(lblDateProspection);
        panDateProspect.add(txtDateProspection);
        panSociete.add(panDateProspect);

        panInteretProspect.setLayout(new BoxLayout(panInteretProspect, BoxLayout.X_AXIS));
        panInteretProspect.add(lblInteret);
        panInteretProspect.add(cmbInteret);
        panSociete.add(panInteretProspect);

        txtCommentaire.setLineWrap(true);
        txtCommentaire.setWrapStyleWord(true);
        panCommentaire.setLayout(new BoxLayout(panCommentaire, BoxLayout.Y_AXIS));
        panCommentaire.add(lblCommentaire);
        panCommentaire.add(txtCommentaire);
        panSociete.add(panCommentaire);

        panCommande.setLayout(new FlowLayout(FlowLayout.CENTER, 200, 10));

        panCommande.add(cmdValider);
        panCommande.add(cmdAnnuler);

        Container contentPane = this.getContentPane();
        contentPane.add(panSociete, BorderLayout.CENTER);
        contentPane.add(panCommande, BorderLayout.SOUTH);

    }

    /**
     * Initialise les composants du formulaire pour créer un nouveau client ou prospect
     *
     */
    public void initCreer(){
        if(FenetrePrincipale.getOptClient().isSelected()) {
        cmdValider.addActionListener(e -> {

            Client client = new Client();
            Adresse adresseC = new Adresse();

            boolean clientValide = true;
            boolean adresseValide = true;

            try{
                adresseC.setNumRue(this.txtNumRue.getText());
                this.lblNumRue.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                adresseValide = false;
                this.lblNumRue.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                adresseC.setNomRue(this.txtNomRue.getText());
                this.lblNomRue.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                adresseValide = false;
                this.lblNomRue.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                adresseC.setCodePostal(this.txtCodePostal.getText());
                this.lblCodePostal.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                adresseValide = false;
                this.lblCodePostal.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                adresseC.setVille(this.txtVille.getText());
                this.lblVille.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                adresseValide = false;
                this.lblVille.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                client.setRaisonSocial(this.txtRaisonSocial.getText());
                this.lblRaisonSocial.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                clientValide = false;
                this.lblRaisonSocial.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                client.setDomaineSociete((DomaineSociete) this.cmbDomaineSociete.getSelectedItem());
                this.lblDomaineSociete.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                clientValide = false;
                this.lblDomaineSociete.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                client.setTelephone(this.txtTelephone.getText());
                this.lblTelephone.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                clientValide = false;
                this.lblTelephone.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                client.setMail(this.txtMail.getText().trim());
                this.lblMail.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                clientValide = false;
                this.lblMail.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            if (adresseValide){
                try {
                    client.setAdresse(adresseC);
                } catch (GestionReversoException gre) {
                    JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
                }
            }

            try{
                client.setCAString(this.txtCA.getText());
                this.lblCA.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                clientValide = false;
                this.lblCA.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                client.setNbEmployesString(this.txtNbEmployes.getText());
                this.lblNbEmployes.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                clientValide = false;
                this.lblNbEmployes.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            if(!txtCommentaire.getText().isEmpty()){
                client.setCommentaire(this.txtCommentaire.getText());
            }

            if(!this.txtCA.getText().isEmpty() && !this.txtNbEmployes.getText().isEmpty() && !this.txtCA.getText().isBlank() && !this.txtNbEmployes.getText().isBlank()){
                if( Long.parseLong(this.txtCA.getText()) > 0 && (Long.parseLong(this.txtCA.getText()) / Integer.parseInt(this.txtNbEmployes.getText())) < 10){
                    clientValide = false;
                    this.lblCA.setForeground(Color.RED);
                    this.lblNbEmployes.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this,"Le chiffre d'affaire divisé par le nombre d'employés doit etre supérieur ou égale à 10.", "Vérification Reverso.", JOptionPane.ERROR_MESSAGE);
                }else{
                    this.lblCA.setForeground(Color.BLACK);
                    this.lblNbEmployes.setForeground(Color.BLACK);
                }
            }

            if(clientValide){
                Client.getClientList().add(client);
                Collections.sort(Client.getClientList(), Client.getComparatorRaisonSocial());

                FenetrePrincipale.updateTableau();
                this.setVisible(false);
                this.homePage.setVisible(true);
            }

        });

        }else if (FenetrePrincipale.getOptProspect().isSelected()) {
            cmdValider.addActionListener(e -> {

            Prospect prospect = new Prospect();
            Adresse adresseP = new Adresse();

            boolean prospectValide = true;
            boolean adresseValide = true;

            try{
                adresseP.setNumRue(this.txtNumRue.getText());
                this.lblNumRue.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                adresseValide = false;
                this.lblNumRue.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                adresseP.setNomRue(this.txtNomRue.getText());
                this.lblNomRue.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                adresseValide = false;
                this.lblNomRue.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                adresseP.setCodePostal(this.txtCodePostal.getText());
                this.lblCodePostal.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                adresseValide = false;
                this.lblCodePostal.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                adresseP.setVille(this.txtVille.getText());
                this.lblVille.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                adresseValide = false;
                this.lblVille.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                prospect.setRaisonSocial(this.txtRaisonSocial.getText());
                this.lblRaisonSocial.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                prospectValide = false;
                this.lblRaisonSocial.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                prospect.setDomaineSociete((DomaineSociete) this.cmbDomaineSociete.getSelectedItem());
                this.lblDomaineSociete.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                prospectValide = false;
                this.lblDomaineSociete.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                prospect.setTelephone(this.txtTelephone.getText());
                this.lblTelephone.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                prospectValide = false;
                this.lblTelephone.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                prospect.setMail(this.txtMail.getText().trim());
                this.lblMail.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                prospectValide = false;
                this.lblMail.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            if (adresseValide){
                try {
                    prospect.setAdresse(adresseP);
                } catch (GestionReversoException gre) {
                    JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
                }
            }

            try{
                prospect.setDateProspectionString(txtDateProspection.getText());
                this.lblCA.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                prospectValide = false;
                this.lblCA.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            try{
                prospect.setInteret((InteretProspect) this.cmbInteret.getSelectedItem());
                this.lblNbEmployes.setForeground(Color.BLACK);
            } catch(GestionReversoException gre){
                prospectValide = false;
                this.lblNbEmployes.setForeground(Color.RED);
                JOptionPane.showMessageDialog(this,gre.getMessage(), "Un champs obligatoire est resté vide.", JOptionPane.ERROR_MESSAGE);
            }

            if(!txtCommentaire.getText().isEmpty()){
                prospect.setCommentaire(this.txtCommentaire.getText());
            }

            if(prospectValide){
                Prospect.getProspectList().add(prospect);
                Collections.sort(Prospect.getProspectList(), Prospect.getComparatorRaisonSocial());

                FenetrePrincipale.updateTableau();
                this.setVisible(false);
                this.homePage.setVisible(true);
            }

        });
    }
        cmdAnnuler.addActionListener(e -> {
            this.setVisible(false);
            homePage.setVisible(true);
        });
    }

    /**
     * Initialise les composants du formulaire pour modifier un client ou prospect
     *
     */
    public void initModifier(){
        if(FenetrePrincipale.getOptClient().isSelected()) {
            txtId.setText(String.valueOf(Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getIdentifiant()));
            txtRaisonSocial.setText(Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getRaisonSocial());
            cmbDomaineSociete.setSelectedItem(Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getDomaineSociete());
            txtNumRue.setText(String.valueOf(Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getAdresse().getNumRue()));
            txtNomRue.setText(Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getAdresse().getNomRue());
            txtCodePostal.setText(Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getAdresse().getCodePostal());
            txtVille.setText(Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getAdresse().getVille());
            txtTelephone.setText(Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getTelephone());
            txtMail.setText(Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getMail());
            txtCommentaire.setText(Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getCommentaire());
            txtCA.setText(String.valueOf(Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getCA()));
            txtNbEmployes.setText(String.valueOf(Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getNbEmployes()));

            cmdValider.addActionListener(e -> {
                boolean clientValide = true;

                try {
                    Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).setRaisonSocial(txtRaisonSocial.getText());
                    this.lblRaisonSocial.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    clientValide = false;
                    this.lblRaisonSocial.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).setDomaineSociete((DomaineSociete) cmbDomaineSociete.getSelectedItem());
                    this.lblDomaineSociete.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    clientValide = false;
                    this.lblDomaineSociete.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getAdresse().setNumRue(txtNumRue.getText());
                    this.lblNumRue.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    clientValide = false;
                    this.lblNumRue.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getAdresse().setNomRue(txtNomRue.getText());
                    this.lblNomRue.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    clientValide = false;
                    this.lblNomRue.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getAdresse().setCodePostal(txtCodePostal.getText());
                    this.lblCodePostal.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    clientValide = false;
                    this.lblCodePostal.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).getAdresse().setVille(txtVille.getText());
                    this.lblVille.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    clientValide = false;
                    this.lblVille.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).setTelephone(txtTelephone.getText());
                    this.lblTelephone.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    clientValide = false;
                    this.lblTelephone.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).setMail(txtMail.getText().trim());
                    this.lblMail.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    clientValide = false;
                    this.lblMail.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }
                if(!txtCommentaire.getText().isEmpty()) {
                    Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).setCommentaire(txtCommentaire.getText());
                }

                if(!this.txtCA.getText().isEmpty() && !this.txtNbEmployes.getText().isEmpty() && !this.txtCA.getText().isBlank() && !this.txtNbEmployes.getText().isBlank()){
                    if( Long.parseLong(this.txtCA.getText()) > 0 && (Long.parseLong(this.txtCA.getText()) / Integer.parseInt(this.txtNbEmployes.getText())) < 10){
                        clientValide = false;
                        this.lblCA.setForeground(Color.RED);
                        this.lblNbEmployes.setForeground(Color.RED);
                        JOptionPane.showMessageDialog(this,"Le chiffre d'affaire divisé par le nombre d'employés doit etre supérieur ou égale à 10.", "Vérification Reverso.", JOptionPane.ERROR_MESSAGE);
                    }else{
                        try{
                            Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).setCAString(txtCA.getText());
                            this.lblCA.setForeground(Color.BLACK);
                        } catch(GestionReversoException gre){
                            clientValide = false;
                            this.lblCA.setForeground(Color.RED);
                            JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                        }

                        try{
                            Client.getClientList().get(FenetrePrincipale.getTblClient().getSelectedRow()).setNbEmployesString(txtNbEmployes.getText());
                            this.lblNbEmployes.setForeground(Color.BLACK);
                        } catch(GestionReversoException gre){
                            clientValide = false;
                            this.lblNbEmployes.setForeground(Color.RED);
                            JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }else{
                    clientValide = false;
                    this.lblCA.setForeground(Color.RED);
                    this.lblNbEmployes.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this,"Le chiffre d'affaire ou le nombre d'employés doit être renseigné.", "Vérification Reverso.", JOptionPane.ERROR_MESSAGE);
                }

                if(clientValide) {
                    FenetrePrincipale.updateTableau();
                    this.setVisible(false);
                    homePage.setVisible(true);
                }
            });

        }else if (FenetrePrincipale.getOptProspect().isSelected()) {
            txtId.setText(String.valueOf(Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getIdentifiant()));
            txtRaisonSocial.setText(Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getRaisonSocial());
            cmbDomaineSociete.setSelectedItem(Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getDomaineSociete());
            txtNumRue.setText(String.valueOf(Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getAdresse().getNumRue()));
            txtNomRue.setText(Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getAdresse().getNomRue());
            txtCodePostal.setText(Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getAdresse().getCodePostal());
            txtVille.setText(Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getAdresse().getVille());
            txtTelephone.setText(Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getTelephone());
            txtMail.setText(Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getMail());
            txtCommentaire.setText(Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getCommentaire());
            txtDateProspection.setText(Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getDateProspection().format(Prospect.getFormatter()));
            cmbInteret.setSelectedItem(Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getInteret());

            cmdValider.addActionListener(e -> {
                boolean prospectValide = true;

                try {
                    Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).setRaisonSocial(txtRaisonSocial.getText());
                    this.lblRaisonSocial.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    prospectValide = false;
                    this.lblRaisonSocial.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).setDomaineSociete((DomaineSociete) cmbDomaineSociete.getSelectedItem());
                    this.lblDomaineSociete.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    prospectValide = false;
                    this.lblDomaineSociete.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getAdresse().setNumRue(txtNumRue.getText());
                    this.lblNumRue.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    prospectValide = false;
                    this.lblNumRue.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getAdresse().setNomRue(txtNomRue.getText());
                    this.lblNomRue.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    prospectValide = false;
                    this.lblNomRue.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getAdresse().setCodePostal(txtCodePostal.getText());
                    this.lblCodePostal.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    prospectValide = false;
                    this.lblCodePostal.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).getAdresse().setVille(txtVille.getText());
                    this.lblVille.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    prospectValide = false;
                    this.lblVille.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).setTelephone(txtTelephone.getText());
                    this.lblTelephone.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    prospectValide = false;
                    this.lblTelephone.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).setMail(txtMail.getText().trim());
                    this.lblMail.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    prospectValide = false;
                    this.lblMail.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }
                if(!txtCommentaire.getText().isEmpty()) {
                    Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).setCommentaire(txtCommentaire.getText());
                }

                try{
                    Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).setDateProspectionString(txtDateProspection.getText());
                    this.lblDateProspection.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    prospectValide = false;
                    this.lblDateProspection.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                try{
                    Prospect.getProspectList().get(FenetrePrincipale.getTblProspect().getSelectedRow()).setInteret((InteretProspect)cmbInteret.getSelectedItem());
                    this.lblInteret.setForeground(Color.BLACK);
                } catch(GestionReversoException gre){
                    prospectValide = false;
                    this.lblInteret.setForeground(Color.RED);
                    JOptionPane.showMessageDialog(this, gre.getMessage(), "Erreur de modification.", JOptionPane.ERROR_MESSAGE);
                }

                if(prospectValide) {
                    FenetrePrincipale.updateTableau();
                    this.setVisible(false);
                    homePage.setVisible(true);
                }
            });
        }
        cmdAnnuler.addActionListener(e -> {
            this.setVisible(false);
            homePage.setVisible(true);
        });


    }
}
