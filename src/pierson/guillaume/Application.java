package pierson.guillaume;


import pierson.guillaume.Reverso.entites.Adresse;
import pierson.guillaume.Reverso.entites.Client;
import pierson.guillaume.Reverso.entites.Prospect;
import pierson.guillaume.exceptions.GestionReversoException;
import pierson.guillaume.fenetres.FenetrePrincipale;
import pierson.guillaume.types.DomaineSociete;
import pierson.guillaume.types.InteretProspect;

import javax.swing.*;
import java.io.IOException;
import java.time.LocalDate;


public class Application {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
/*
        try {
            Client c1 = new Client(
                    "Microsoft",
                    DomaineSociete.PRIVE,
                    "0678901234",
                    "contact@hotmail.com",
                    "Grosse boite",
                    new Adresse(
                            "35",
                            "rue des fenetres",
                            "92123",
                            "Windowcity"),
                    1258000000,
                    166175
            );

            Client c2 = new Client(
                    "Apple",
                    DomaineSociete.PRIVE,
                    "+33612345678",
                    "contact@mac.com",
                    "Grosse boite aussi",
                    new Adresse(
                            "53",
                            "rue des pommes",
                            "54123",
                            "Maccity"),
                    32556483,
                    15
            );

            Prospect p1 = new Prospect(
                    "Domino's",
                    DomaineSociete.PRIVE,
                    "06 12 34 56 78",
                    "contact@dominos.fr",

                    new Adresse(
                            "65",
                            "rue des Pizza",
                            "45123",
                            "Pizzacity"),
                    LocalDate.parse("20-06-2020", Prospect.getFormatter()),
                    InteretProspect.OUI
            );
        }
        catch (GestionReversoException gre){
            JOptionPane.showMessageDialog(null, gre.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
        }
*/
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e){ }
        new Application();

    }

    /**
     * Constructeur de l'application
     */
    public Application() throws IOException, ClassNotFoundException {
        FenetrePrincipale frame = new FenetrePrincipale();
        frame.setVisible(true);
    }
}
