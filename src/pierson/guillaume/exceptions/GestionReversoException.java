package pierson.guillaume.exceptions;

public class GestionReversoException extends Exception {

    public GestionReversoException(String message) {
        super(message);
    }

    /**
     *
     * @param objet
     * @param nomParam
     * @param <T>
     * @return
     * @throws GestionReversoException
     */
    public static <T> T testNonNull(T objet, String nomParam)
            throws GestionReversoException
    {
        if (objet == null) {
            throw new GestionReversoException(nomParam + " doit être renseigné (non Null). Valeur reçu : \" " + objet + " \".");
        }
        return objet;
    }

    public static String testNiNullNiVide(
            String str,
            String nomParam
    ) throws GestionReversoException
    {
        if (str == null || str.isEmpty()) {
            throw new GestionReversoException(
                    nomParam + " doit être renseigné (ni Null ni vide). Valeur reçu : \" " + str + " \"."
            );
        }

        return str;
    }
}
