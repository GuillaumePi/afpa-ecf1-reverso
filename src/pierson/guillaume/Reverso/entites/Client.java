package pierson.guillaume.Reverso.entites;

import pierson.guillaume.types.DomaineSociete;
import pierson.guillaume.exceptions.GestionReversoException;

import java.util.*;
import java.io.Serializable;

public class Client extends Societe implements Serializable {
    private static final long serialVersionUID = -2893755735859018262L;

    private long CA;
    private int nbEmployes;
    private static ArrayList<Client> clientList = new ArrayList<Client>();

    /**
     * Constructeur par defaut
     */
    public Client(){
        super();
    }

    /**
     * Constructeur de Client
     * lance une exception si le CA est supérieur à zéro et que le rapport CA/le nombre d'employés est inférieur à 10
     * ajoute à la liste
     * trie la liste
     * @param raisonSocial
     * @param domaineSociete
     * @param telephone
     * @param mail
     * @param commentaire
     * @param adresse
     * @param CA
     * @param nbEmployes
     * @throws GestionReversoException
     */
    public Client(String raisonSocial, DomaineSociete domaineSociete, String telephone, String mail, String commentaire, Adresse adresse, long CA, int nbEmployes) throws GestionReversoException {
        super(raisonSocial, domaineSociete, telephone, mail, commentaire, adresse);
        if (CA > 0 && ((CA/nbEmployes)<10)){
            throw new GestionReversoException("Erreur");
        } else {
            this.setCA(CA);
            this.setNbEmployes(nbEmployes);

            clientList.add(this);
            Collections.sort(clientList, Client.ComparatorRaisonSocial);
        }
    }

    /**
     * Constructeur de Client sans commentaire
     * lance une exception si le CA est supérieur à zéro et que le rapport CA/le nombre d'employés est inférieur à 10
     * ajoute à la liste
     * trie la liste
     * @param raisonSocial
     * @param domaineSociete
     * @param telephone
     * @param mail
     * @param adresse
     * @param CA
     * @param nbEmployes
     * @throws GestionReversoException
     */
    public Client(String raisonSocial, DomaineSociete domaineSociete, String telephone, String mail, Adresse adresse, long CA, int nbEmployes) throws GestionReversoException {
        super(raisonSocial, domaineSociete, telephone, mail, adresse);
        if (CA > 0 && ((CA/nbEmployes)<10)){
            throw new GestionReversoException("Erreur");
        } else {
            this.setCA(CA);
            this.setNbEmployes(nbEmployes);
            clientList.add(this);
            Collections.sort(clientList, Client.ComparatorRaisonSocial);
        }
    }

    public long getCA() {
        return CA;
    }

    /**
     * Setter du chiffre d'affaire
     * lance une exception si null ou inferieur à zéro
     * @param CA
     * @throws GestionReversoException
     */
    public void setCA(long CA) throws GestionReversoException {
        GestionReversoException.testNonNull(CA, "Le chiffre d'affaire ");
        if (CA < 0) {
            throw new GestionReversoException("Le chiffre d'affaire doit être renseigné et doit etre positif ou égal à 0. Valeur reçu : \" " + CA + " \".");
        } else {
            this.CA = Objects.requireNonNull(CA);
        }
    }

    /**
     * Setter du chiffre d'affaire avec une String en parametre
     * lance une exception si null ou inferieur à zéro
     * @param CA
     * @throws GestionReversoException
     */
    public void setCAString(String CA) throws GestionReversoException {
        GestionReversoException.testNiNullNiVide(CA, "Le chiffre d'affaire ");
        if (Long.parseLong(CA) < 0) {
            throw new GestionReversoException("Le chiffre d'affaire doit être renseigné et doit etre positif ou égal à 0. Valeur reçu : \" " + CA + " \".");
        } else {
            this.CA = Objects.requireNonNull(Long.parseLong(CA));
        }
    }

    public int getNbEmployes() {
        return nbEmployes;
    }

    /**
     * Setter du nombre d'employés
     * lance une exception si null ou inférieur ou égal à zéro
     * @param nbEmployes
     * @throws GestionReversoException
     */
    public void setNbEmployes(int nbEmployes) throws GestionReversoException {
        GestionReversoException.testNonNull(nbEmployes, "Le nombre d'employés ");
        if (nbEmployes <= 0) {
            throw new GestionReversoException("Le nombre d'employés doit être renseigné et doit etre supérieur à 0. Valeur reçu : \" " + nbEmployes + " \".");
        } else {
            this.nbEmployes = Objects.requireNonNull(nbEmployes);
        }
    }

    /**
     * Setter du nombre d'employés avec une String en parametre
     * lance une exception si null ou inférieur ou égal à zéro
     * @param nbEmployes
     * @throws GestionReversoException
     */
    public void setNbEmployesString(String nbEmployes) throws GestionReversoException {
        GestionReversoException.testNiNullNiVide(nbEmployes, "Le nombre d'employés ");
        if (Integer.parseInt(nbEmployes) <= 0) {
            throw new GestionReversoException("Le nombre d'employés doit être renseigné et doit etre supérieur à 0. Valeur reçu : \" " + nbEmployes + " \".");
        } else {
            this.nbEmployes = Objects.requireNonNull(Integer.parseInt(nbEmployes));
        }
    }


    public static ArrayList<Client> getClientList() {
        return clientList;
    }

    public static void setClientList(ArrayList<Client> clientList) {
        Client.clientList = clientList;
    }

    public static Comparator<Client> getComparatorRaisonSocial() {
        return ComparatorRaisonSocial;
    }

    @Override
    public String toString() {
        return super.toString() + "\n" +
                "CA : " + CA + "\n" +
                "Nombre d'employes : " + nbEmployes + "\n"
                ;
    }

    /**
     * Comparator pour le tri des clients par raison social
     */
    private static Comparator<Client> ComparatorRaisonSocial = new Comparator<Client>() {
        @Override
        public int compare(Client c1, Client c2) {
            return c1.getRaisonSocial().compareTo(c2.getRaisonSocial());
        }
    };
}
