package pierson.guillaume.Reverso.entites;

import pierson.guillaume.exceptions.GestionReversoException;
import pierson.guillaume.types.DomaineSociete;

import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Pattern;

public abstract class Societe implements Serializable {

    private static final long serialVersionUID = -3382851697071092339L;

    /**
     * Rgex pour l'adresse mail.
     */
    private static final Pattern EMAIL_PATTERN
            = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
    /** TODO a ameliorer
     * Regex pour le numérode téléphone. Accepte :
     * 0612345678
     * +33612345678
     * +357123456
     * 03 12 34 56 78
     * +336 12 34 56 78
     * +356 123 456
     * 03-12-34-56-78
     * 03/12/34/56/78
     * 03.12.34.56.78
     */
    private static final Pattern TELEPHONE_PATTERN
            = Pattern.compile("(^\\+*[0-9]{9,11}+$)|(^\\+*[0-9]{2,3}+[.\\-\\/ ]+[0-9]{2}+[.\\-\\/ ]+[0-9]{2}+[.\\-\\/ ]+[0-9]{2}+[.\\-\\/ ]+[0-9]{2}+$)|(^\\+*[0-9]{3}+[.\\-\\/ ]+[0-9]{3}+[.\\-\\/ ]+[0-9]{3}+$)");


    private int identifiant;
    private String raisonSocial;
    private DomaineSociete domaineSociete;
    private String telephone;
    private String mail;
    private String commentaire;
    private Adresse adresse;

    private static int compteurId = 1;

    /**
     * Constructeur par defaut
     */
    public Societe(){
        this.identifiant = compteurId++;
    }

    /**
     * Constructeur de Societe
     * @param raisonSocial
     * @param domaineSociete
     * @param telephone
     * @param mail
     * @param commentaire
     * @param adresse
     * @throws GestionReversoException
     */
    public Societe(String raisonSocial, DomaineSociete domaineSociete, String telephone, String mail, String commentaire, Adresse adresse) throws GestionReversoException{
        this.identifiant = compteurId++;

        this.setRaisonSocial(raisonSocial);
        this.setDomaineSociete(domaineSociete);
        this.setTelephone(telephone);
        this.setMail(mail);
        this.setCommentaire(commentaire);
        this.setAdresse(adresse);

    }

    /**
     * Constructeur de Societe sans commentaire
     * @param raisonSocial
     * @param domaineSociete
     * @param telephone
     * @param mail
     * @param adresse
     * @throws GestionReversoException
     */
    public Societe(String raisonSocial, DomaineSociete domaineSociete, String telephone, String mail, Adresse adresse) throws GestionReversoException{
        this.identifiant = compteurId++;

        this.setRaisonSocial(raisonSocial);
        this.setDomaineSociete(domaineSociete);
        this.setTelephone(telephone);
        this.setMail(mail);
        this.setAdresse(adresse);

    }

    public Pattern getEmailPattern() {
        return EMAIL_PATTERN;
    }

    public Pattern getTelephonePattern() {
        return TELEPHONE_PATTERN;
    }

    public int getIdentifiant() {
        return identifiant;
    }

    public String getRaisonSocial() {
        return raisonSocial;
    }

    /**
     * Setter de la raison social
     * lance une exception si null ou vide
     * @param raisonSocial
     * @throws GestionReversoException
     */
    public void setRaisonSocial(String raisonSocial) throws GestionReversoException{
        GestionReversoException.testNiNullNiVide(raisonSocial, "La raison social");
        this.raisonSocial = Objects.requireNonNull(raisonSocial);
    }

    public DomaineSociete getDomaineSociete() {
        return domaineSociete;
    }

    /**
     * Setter du domaine de la societe
     * lance une exception si null ou different des choix de l'enumération DomaineSociete
     * @param domaineSociete
     * @throws GestionReversoException
     */
    public void setDomaineSociete(DomaineSociete domaineSociete) throws GestionReversoException {
        if (domaineSociete == null || (!domaineSociete.equals(DomaineSociete.PRIVE) && !domaineSociete.equals(DomaineSociete.PUBLIC))) {
            throw new GestionReversoException("Le domaine de la société doit être renseigné et ne peut être que <<PRIVE>> ou <<PUBLIC>>. Valeur reçu : \" " + domaineSociete + " \".");
        }
        this.domaineSociete = Objects.requireNonNull(domaineSociete);
    }


    public String getTelephone() {
        return telephone;
    }
    /**
     * Setter du numéro de téléphone
     * lance une exception si null ou vide
     * vérifie le Regex du numéro de téléphone
     * @param telephone
     * @throws GestionReversoException
     */
    public void setTelephone(String telephone) throws GestionReversoException {
        GestionReversoException.testNiNullNiVide(telephone, "Le numéro de téléphone");

        if (!TELEPHONE_PATTERN.matcher(telephone).matches()) {
            throw new GestionReversoException(
                    "Ce numéro de téléphone n'est pas valide: \" " + telephone + " \".");
        }

        this.telephone = Objects.requireNonNull(telephone);
    }

    public String getMail() {
        return mail;
    }

    /**
     * Setter de l'adresse mail
     * lance une exception si null ou vide.
     * vérifie le regex de l'adresse mail.
     * @param mail
     * @throws GestionReversoException
     */
    public void setMail(String mail) throws GestionReversoException{
        GestionReversoException.testNiNullNiVide(mail, "Adresse mail");

        if (!EMAIL_PATTERN.matcher(mail).matches()) {
            throw new GestionReversoException(
                    "Cette adresse mail n'est pas valide: \" " + mail + " \".");
        }
        this.mail = Objects.requireNonNull(mail);
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    /**
     * Setter de l'objet adresse
     * lance une exception si null
     * @param adresse
     * @throws GestionReversoException
     */
    public void setAdresse(Adresse adresse) throws GestionReversoException {
        GestionReversoException.testNonNull(adresse, "L'adresse");
        this.adresse = Objects.requireNonNull(adresse);
    }

    public static int getCompteurId() {
        return compteurId;
    }

    public static void setCompteurId(int compteurId) {
        Societe.compteurId = compteurId;
    }

    @Override
    public String toString() {
        return "Identifiant : " + identifiant + "\n" +
                "Raison social : " + raisonSocial + "\n" +
                "Domaine : " + domaineSociete + "\n" + "\n" +
                "Adresse : " + adresse + "\n" +
                "Téléphone : " + telephone + "\n" +
                "Adresse mail : " + mail + "\n" + "\n" +
                "Commentaire : " + commentaire + "\n"
                ;
    }


}
