package pierson.guillaume.Reverso.entites;

import pierson.guillaume.types.DomaineSociete;
import pierson.guillaume.exceptions.GestionReversoException;
import pierson.guillaume.types.InteretProspect;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.io.Serializable;

public class Prospect extends Societe implements Serializable {
    private static final long serialVersionUID = 7262839861191670563L;

    private LocalDate dateProspection;
    private InteretProspect interet;
    private static ArrayList<Prospect> prospectList = new ArrayList<Prospect>();

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    /**
     * Constructeur par defaut
     */
    public Prospect(){
        super();
    }

    /**
     * Constructeur de Prospect
     * ajoute à la liste
     * trie la liste
     * @param raisonSocial
     * @param domaineSociete
     * @param telephone
     * @param mail
     * @param commentaire
     * @param adresse
     * @param dateProspection
     * @param interet
     * @throws GestionReversoException
     */
    public Prospect(String raisonSocial, DomaineSociete domaineSociete, String telephone, String mail, String commentaire, Adresse adresse, LocalDate dateProspection, InteretProspect interet) throws GestionReversoException {
        super(raisonSocial, domaineSociete, telephone, mail, commentaire, adresse);

        this.setDateProspection(dateProspection);
        this.setInteret(interet);

        prospectList.add(this);
        Collections.sort(prospectList, Prospect.ComparatorRaisonSocial);
    }

    /**
     * Constructeur de Prospect sans commentaire
     * ajoute à la liste
     * trie la liste
     * @param raisonSocial
     * @param domaineSociete
     * @param telephone
     * @param mail
     * @param adresse
     * @param dateProspection
     * @param interet
     * @throws GestionReversoException
     */
    public Prospect(String raisonSocial, DomaineSociete domaineSociete, String telephone, String mail, Adresse adresse, LocalDate dateProspection, InteretProspect interet) throws GestionReversoException {
        super(raisonSocial, domaineSociete, telephone, mail, adresse);

        this.setDateProspection(dateProspection);
        this.setInteret(interet);

        prospectList.add(this);
        Collections.sort(prospectList, Prospect.ComparatorRaisonSocial);
    }

    public LocalDate getDateProspection() {
        return dateProspection;
    }

    /**
     * Setter de la date de la derniere prospection
     * lance une exception si null ou après aujourd'hui
     * @param dateProspection
     * @throws GestionReversoException
     */
    public void setDateProspection(LocalDate dateProspection) throws GestionReversoException {
        if (dateProspection == null || dateProspection.isAfter(LocalDate.now())){
            throw new GestionReversoException("La date de la dernière prospection doit être renseigné et ne peut pas être plus tard qu'aujourd'hui! Valeur reçu : \" " + dateProspection + " \".");
        } else {
            this.dateProspection = Objects.requireNonNull(dateProspection);
        }
    }

    public void setDateProspectionString(String dateProspection) throws GestionReversoException {
        if (dateProspection == null || LocalDate.parse(Objects.requireNonNull(dateProspection), getFormatter()).isAfter(LocalDate.now())){
            throw new GestionReversoException("La date de la dernière prospection doit être renseigné et ne peut pas être plus tard qu'aujourd'hui! Valeur reçu : \" " + dateProspection + " \".");
        } else {
            this.dateProspection = LocalDate.parse(Objects.requireNonNull(dateProspection), getFormatter());
        }
    }

    public InteretProspect getInteret() {
        return interet;
    }

    /**
     * Setter de l'interet du prospect
     * lance une exception si null ou different des choix de l'enumération InteretProspect
     * @param interet
     * @throws GestionReversoException
     */
    public void setInteret(InteretProspect interet) throws GestionReversoException {
        if (interet == null || (!interet.equals(InteretProspect.OUI) && !interet.equals(InteretProspect.NON))){
            throw new GestionReversoException("La valeur de l'interet doit être renseigné et ne peut être que <<OUI>> ou <<NON>>. Valeur reçu : \" " + interet + " \".");
        } else {
            this.interet = Objects.requireNonNull(interet);
        }
    }

    public static ArrayList<Prospect> getProspectList() {
        return prospectList;
    }

    public static void setProspectList(ArrayList<Prospect> prospectList) {
        Prospect.prospectList = prospectList;
    }

    public static DateTimeFormatter getFormatter() {
        return formatter;
    }

    public static Comparator<Prospect> getComparatorRaisonSocial() {
        return ComparatorRaisonSocial;
    }


    @Override
    public String toString() {
        return super.toString() + "\n" +
                "Date de la dernière prospection : " + dateProspection.format(formatter) + "\n" +
                "Interressé : " + interet + "\n"
                ;
    }

    /**
     * Comparator pour le tri des prospects par raison social
     */
    private static Comparator<Prospect> ComparatorRaisonSocial = new Comparator<Prospect>() {
        @Override
        public int compare(Prospect p1, Prospect p2) {
            return p1.getRaisonSocial().compareTo(p2.getRaisonSocial());
        }
    };
}


