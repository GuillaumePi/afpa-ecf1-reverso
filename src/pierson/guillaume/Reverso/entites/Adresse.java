package pierson.guillaume.Reverso.entites;

import pierson.guillaume.exceptions.GestionReversoException;

import java.io.Serializable;
import java.util.Objects;

public class Adresse implements Serializable {
    private static final long serialVersionUID = 7948022485023711326L;

    private int numRue;
    private String nomRue;
    private String codePostal;
    private String ville;

    /**
     * Constructeur par defaut
     */
    public Adresse(){

    }

    /**
     * Constructeur d'Adresse
     * @param numRue
     * @param nomRue
     * @param codePostal
     * @param ville
     * @throws GestionReversoException
     */
    public Adresse(String numRue, String nomRue, String codePostal, String ville) throws GestionReversoException {
        this.setNumRue(numRue);
        this.setNomRue(nomRue);
        this.setCodePostal(codePostal);
        this.setVille(ville);
    }

    public int getNumRue() {
        return numRue;
    }

    /**
     * Setter du numéro de rue
     * lance une exception si null
     * @param numRue
     * @throws GestionReversoException
     */
    public void setNumRue(String numRue) throws GestionReversoException {
        GestionReversoException.testNiNullNiVide(numRue, "Le numéro de rue");
        this.numRue = Objects.requireNonNull(Integer.parseInt(numRue));
    }

    public String getNomRue() {
        return nomRue;
    }

    /**
     * Setter du nom de rue
     * lance une exception si null ou vide
     * @param nomRue
     * @throws GestionReversoException
     */
    public void setNomRue(String nomRue) throws GestionReversoException {
        GestionReversoException.testNiNullNiVide(nomRue, "Le nom de rue");
        this.nomRue = Objects.requireNonNull(nomRue);
    }

    public String getCodePostal() {
        return codePostal;
    }

    /**
     * Setter du code postal
     * lance une exception si null ou vide
     * @param codePostal
     * @throws GestionReversoException
     */
    public void setCodePostal(String codePostal) throws GestionReversoException {
        GestionReversoException.testNiNullNiVide(codePostal, "Le code postal");
        this.codePostal = Objects.requireNonNull(codePostal);
    }

    public String getVille() {
        return ville;
    }

    /**
     * Setter de la ville
     * lance une exception si null ou vide
     * @param ville
     * @throws GestionReversoException
     */
    public void setVille(String ville) throws GestionReversoException {
        GestionReversoException.testNiNullNiVide(ville, "La ville");
        this.ville = Objects.requireNonNull(ville);
    }

    @Override
    public String toString() {
        return "\n" + numRue + " " + nomRue + "\n" +
               codePostal + " " + ville + "\n"
                ;
    }
}
